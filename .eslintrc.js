/*
 * @Author: rzh
 * @Date: 2021-10-10 18:47:43
 * @LastEditors: rzh
 * @LastEditTime: 2021-10-10 18:48:03
 * @Description: Do not edit
 */

module.exports = {
    "env": {
        "browser": true,
        "es6": true,
        "node": true
    },
    "extends": "eslint:recommended",
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly",
        "ENV": true
    },
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "rules": {
        "linebreak-style": [
          "error",
          "unix"
        ],
        "quotes": [
          "error",
          "single"
        ]
    }
};
